class Zshunit < Formula
  desc "xUnit testing for ZSH scripts"
  homepage "https://bitbucket.org/xavist/zshunit"
  url "https://xavist@bitbucket.org/xavist/zshunit.git", :tag => "1.0.0"
  sha256 ""

  def install
    bin.install "zshunit" 
    bin.install Dir["lib"]
    prefix.install Dir["tests"]
  end

  test do
    system "#{bin}/zshunit", "#{prefix}/tests/*"
  end
end
